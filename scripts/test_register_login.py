
import pytest
import requests

from api.req_login_api import RegLoginApi
from common.utils import Assert_utils, clear_user_info, read_json
from parameterized import parameterized
import time

@pytest.mark.run(order = 1)
class TestRegLogin(object):

    def setup_class(self):
        clear_user_info()



    def setup(self):
        self.login = requests.session()
        # 2.实例化封装接口请求的类，获取类的对象


    # 获取图片验证码测试用例
    @pytest.mark.parametrize("r,exp_status_code",read_json("reg_login_data", "img_verify_code"))
    # @parameterized.expand(read_json("reg_login_data", "img_verify_code"))
    def test01_img_verify_code(self,r,exp_status_code):
        # 1.创建session对象
        # 3.调用实例化类的对象，获取封装注册图片验证码接口请求的方法，拿到该接口的响应数据
        req = RegLoginApi.get_img_verify_code(self.login,r)
        # 4.断言
        assert exp_status_code == req.status_code

    # 获取短信验证码测试用例
    @parameterized.expand(read_json("reg_login_data","phone_verify_code"))
    def test02_phone_verify_code(self,phone,imgVerifyCode,exp_status,description):

        # 3.调用实例化类的对象，实现获取注册图片验证码接口请求
        RegLoginApi.get_img_verify_code(self.login,"1231231")
        # 4.调用实例化类的对象，实现获取注册短信验证码接口请求，拿到该接口的响应数据
        a=RegLoginApi.get_phone_verify_code(self.login,phone,imgVerifyCode,"reg")
        # 5.断言
        # 断言状态码
        assert 200 == a.status_code
        # 断言响应体数据
        # Assert_utils.assert_utils(a,exp_status,description)
        assert exp_status == a.json().get("status")
        assert description in a.json().get("description")

    # 注册测试用例
    @parameterized.expand(read_json("reg_login_data", "user_register"))
    def test03_register(self,form_dict,exp_status,description):
        # 1.实现获取注册图片验证码接口
        RegLoginApi.get_img_verify_code(self.login,"1231231")

        # 2.实现获取注册短信验证码接口
        RegLoginApi.get_phone_verify_code(self.login,form_dict.get("phone"), "8888", "reg")
        # 3.调用封装注册接口请求的方法，获取响应数据
        body_register = form_dict
        register=RegLoginApi.user_register(self.login,body_register)
        # 4.断言
        Assert_utils.assert_utils(register,exp_status,description)

    # 登录测试用例
    @parameterized.expand(read_json("reg_login_data", "user_login"))
    def test04_user_login(self,login,exp_status,description):
        # 1.调用封装登录接口请求的方法，获取响应数据
        login=RegLoginApi.user_login(self.login,login)
        # 2.断言
        Assert_utils.assert_utils(login,exp_status,description)

    @pytest.mark.skipif()
    def test05_password(self):
        password = RegLoginApi.user_login(self.login,{"keywords": "17843264321","password": "aaaaaa"})
        Assert_utils.assert_utils(password,100,"密码错误1次")
        time.sleep(2)
        password = RegLoginApi.user_login(self.login,{"keywords": "17843264321", "password": "aaaaaa"})
        Assert_utils.assert_utils(password, 100, "密码错误2次")
        time.sleep(2)
        password = RegLoginApi.user_login(self.login,{"keywords": "17843264321", "password": "aaaaaa"})
        Assert_utils.assert_utils(password, 100, "已被锁定")

        time.sleep(60)

        password = RegLoginApi.user_login(self.login,{"keywords": "17843264321", "password": "qqq123"})
        Assert_utils.assert_utils(password, 200, "成功")