import pytest
import requests

from api.open_api import OpenAccountApi
from api.req_login_api import RegLoginApi
from common.utils import Assert_utils, html_util

@pytest.mark.run(order = 2)
class Test_open:

    def setup(self):
        self.ses = requests.Session()
        self.open = OpenAccountApi(self.ses)
        RegLoginApi.user_login(self.ses, {"keywords": "17843264321", "password": "qqq123"})



    def test01_real(self):
        resp = self.open.realname("张三","450102199003078798")
        assert 200 == resp.status_code


    def test02_open_account(self):
        resp = self.open.open_account()
        Assert_utils.assert_utils(resp,200,"form")


    def test03_third_oprn_account(self):
        resp_open = self.open.open_account()
        data_list = html_util(resp_open)
        url = data_list[0]
        form_dict = data_list[1]

        resp = self.open.third_open_account(url,form_dict)

        assert 200 == resp.status_code
        assert "OK" in resp.text

