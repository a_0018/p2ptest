import pytest
import requests

from api.req_login_api import RegLoginApi
from api.top_api import RechargeApi
from common.utils import Assert_utils, html_util, read_json
from parameterized import parameterized

@pytest.mark.run(order = 3)
class TestRecharge(object):

    def setup(self):
        # 创建session对象
        self.ses = requests.Session()
        # 实例化登录模块封装的类
        RegLoginApi.user_login(self.ses,{"keywords": "17843264321", "password": "qqq123"})
        # 实例化充值模块封装的类
        self.top = RechargeApi(self.ses)

    # 获取充值验证码测试用例
    @parameterized.expand(read_json("recharge_data","recharge_verify_code"))
    def test01_get_recharge_verify_code(self,r,exp_status_code):
        # 获取充值图片验证码
        top_1 = self.top.get_recharge_verify_code(r)
        # 断言
        assert exp_status_code == top_1.status_code

    # 充值测试用例
    # @parameterized.expand(read_json("recharge_data","recharge"))
    @pytest.mark.parametrize("money,code,exp_status,exp_description",read_json("recharge_data","recharge"))
    def test02_recharge(self,money,code,exp_status,exp_description):
        #登录
        #获取充值图片验证码
        self.top.get_recharge_verify_code("0.1234566")
        #充值
        top_2 = self.top.recharge(money,code)
        #断言
        # assert exp_status == top_2.status_code
        Assert_utils.assert_utils(top_2,exp_status,exp_description)

    # 第三方充值测试用例
    def test03_third_recharge(self):
        # 登录
        # 获取充值图片验证码
        self.top.get_recharge_verify_code("0.1234566")
        # 充值
        resp_top = self.top.recharge("20000","8888")
        data_list = html_util(resp_top)
        # 获取第三方开户接口的url
        url = data_list[0]
        # 获取第三方开户接口的请求体数据
        form_dict = data_list[1]
        # 获取第三方接口返回的响应结果
        top = self.top.third_recharge(url,form_dict)
        #断言
        assert 200 == top.status_code
        assert "OK" in top.text