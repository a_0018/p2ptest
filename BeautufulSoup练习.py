# 1、导包
from bs4 import BeautifulSoup

data = """
<html> 
    <head>
        <title>黑马程序员</title>
    </head> 
    <body>
        <p id="test01">软件测试</p>
        <p id="test02">2020年</p>
        <a href="/api.html">接口测试</a>
        <a href="/web.html">Web自动化测试</a> 
        <a href="/app.html">APP自动化测试</a>
</body>
</html>
"""
# 2、获取BeautifulSoup对象
bs=BeautifulSoup(data,"html.parser")
# 3、调用相关方法
# 获取title标签的全部数据
print(bs.title)
# # 获取title标签的内容
print(bs.title.string)
# # 获取p标签的第一条数据，id属性的值
print(bs.p.get("id"))
# 获取所有a标签
print(bs.findAll("a"))
# 获取所有a标签的href属性的值,并存入列表中
#1、定义一个新列表，用来接收获取href属性的值
aa = bs.findAll("a")
list_1 = []
for bb in aa:
    print(bb)
    cc=bs.a.get("href")
    list_1.append(cc)
print(list_1)