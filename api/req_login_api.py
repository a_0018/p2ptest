from config import BASE_HOST


class RegLoginApi:

    # def __init__(self,ses):
    #     self.ses = ses

    # 获取图片验证码接口
    @classmethod
    def get_img_verify_code(cls,ses,r):
        url_picture = BASE_HOST + f"/common/public/verifycode1/{r}"
        picture = ses.get(url = url_picture)
        print(f"图片验证码返回的状态码为{picture.status_code}")
        return picture

    # 获取短信验证码接口
    @classmethod
    def get_phone_verify_code(cls,ses, phone_v, img_code_v, type_v):
        url_note = BASE_HOST + "/member/public/sendSms"
        form_dict = {"phone": phone_v,"imgVerifyCode":img_code_v,"type": type_v}
        note = ses.post(url = url_note,data=form_dict)
        print(f"短信验证码返回的状态码为{note.status_code}",f"响应体为:{note.json().get('description')}")
        return note

    # 注册接口
    @classmethod
    def user_register(cls,ses,form_dict):
        url_register = BASE_HOST + "/member/public/reg"
        register = ses.post(url=url_register,data=form_dict)
        print(f"注册接口返回的状态码为{register.status_code}，响应体为{register.json()}")
        return register

    # 登录接口
    @classmethod
    def user_login(cls, ses,longin_v):
        url_login = BASE_HOST + "/member/public/login"
        longin = ses.post(url=url_login,data=longin_v)
        print(f"登录接口返回的状态码为{longin.status_code}，响应体为{longin.json()}")
        return longin