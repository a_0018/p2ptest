from config import BASE_HOST


class OpenAccountApi:
    # 定义一个类属性，用来接收实例化参数传入的session对象
    def __init__(self,ses):
        self.ses = ses

    # 实名认证接口
    def realname(self,realname_v,cardId_v):
        # 实现接口请求，获取响应结果
        url = BASE_HOST + "/member/realname/approverealname"
        form_dict = {"realname":realname_v,"card_id":cardId_v}
        resp = self.ses.post(url=url,data=form_dict,files = {"a":"b"})
        # 封装方法的返回
        print(f"获取实名认证返回的响应数据为：{resp.json()}")
        return resp

    # 开户接口
    def open_account(self):
        # 实现接口请求，获取响应结果
        url = BASE_HOST + "/trust/trust/register"
        # 封装方法的返回
        resp = self.ses.post(url=url)
        print(f"开户接口返回响应数据为：{resp.text}")
        return resp

    # 第三方开户接口
    def third_open_account(self, url, form_dict):
        # 实现接口请求，获取响应结果
        resp = self.ses.post(url=url,data=form_dict)
        print(f"第三方返回响应结果：{resp.text}")
        # 封装方法的返回
        return resp
