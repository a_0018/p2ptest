import requests

from config import BASE_HOST


class RechargeApi:

    def __init__(self,ses):
        self.ses = ses
    # 接收session对象的变量

    # 获取充值验证码接口
    def get_recharge_verify_code(self, r):
        # 定义一个变量，用来接收接口请求得url
        url = BASE_HOST + f"/common/public/verifycode/{r}"
        # 实现接口请求，获取响应数据
        resp = self.ses.get(url=url)
        print(f"充值验证码接口返回的响应体数据为：{resp}")
        # 返回结果
        return resp

    # 充值接口
    def recharge(self,money,code):
        # 定义一个变量，用来接收接口请求得url
        url = BASE_HOST + "/trust/trust/recharge"
        # 定义一个字典变量，用来接收请求体数据
        form_dict = {"paymentType":"chinapnrTrust","amount":money,"formStr":"reForm","valicode":code}
        # 实现接口请求，获取响应数据
        resp = self.ses.post(url=url,data=form_dict)
        print(f'获取充值响应接口的返回响应体为：{resp.text}')
        # 返回结果
        return resp

    # 第三方充值接口
    def third_recharge(self, url, form_dict):
        # 实现接口请求，获取响应数据
        resp = self.ses.post(url=url,data=form_dict)
        print(f"获取第三方响应体数据为：{resp.text}")
        # 返回结果
        return resp
