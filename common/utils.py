import json

from bs4 import BeautifulSoup

from config import BASE_DIR


class Assert_utils:
    @classmethod
    def assert_utils(self, a1, status, description):
        assert 200 == a1.status_code
        assert status == a1.json().get("status")
        assert description in a1.json().get("description")


# 导包
import pymysql
import traceback  # python自带错误信息打印工具包


# 1.封装操作数据库的方法
def exe_sql(sql_str, db=None):
    # 定义两个变量，用来接收之后创建的数据库连接对象，游标对象
    conn = None
    cur = None
    try:
        # 2.创建数据库连接对象
        conn = pymysql.connect(host="121.43.169.97", user="student", password="P2P_student_2022", database=db)
        # 3.创建游标对象
        cur = conn.cursor()
        # 4.在游标对象中执行SQL语句
        cur.execute(sql_str)
        # 如果是查询语句
        if sql_str.split(" ")[0].lower() == "select":
            # 返回全部查询结果
            return cur.fetchall()
        # 如果是非查询语句
        else:
            # 手动提交数据事务
            conn.commit()
            # 返回受影响的行数
            return "受影响的行数：{}".format(cur.rowcount)

    except:
        if conn is not None:
            # 有异常的情况下，进行数据库事务回滚
            conn.rollback()
        # 错误日志的打印
        traceback.print_exc()
    finally:
        # 5.关闭游标对象
        if cur is not None:
            cur.close()
        # 6.关闭数据库连接对象
        if conn is not None:
            conn.close()


# 封装清洗用户信息的方法
def clear_user_info():
    reg_mobile = "'17843264321','17843264322','17843264323','17843264324','17843264325','17843264326','1784326432','17843264329','17843264330'"
    # 需要执行SQL语句8
    sql1 = "delete i.* from mb_member_info i inner join mb_member m on m.id = i.member_id where m.phone in ({});".format(
        reg_mobile)
    sql2 = "delete l.* from mb_member_login_log l inner join mb_member m on m.id=l.member_id where m.phone in ({});".format(
        reg_mobile)
    sql3 = "delete from mb_member where phone in ({});".format(reg_mobile)
    sql4 = "delete from mb_member_register_log where phone in ({});".format(reg_mobile)
    # 调用封装操作数据库的方法，执行SQL语句
    print(exe_sql(sql1, "czbk_member"))
    print(exe_sql(sql2, "czbk_member"))
    print(exe_sql(sql3, "czbk_member"))
    print(exe_sql(sql4, "czbk_member"))


# 封装读取测试数据的方法
def read_json(api, apiname):
    # 获取测试数据的文件路径
    file_path = BASE_DIR + f"/data/{api}.json"
    # 读取测试数据
    with open(file_path, encoding="utf-8") as f:
        # 将测试数据解析为字典类型,获取指定接口的测试数据
        api_data = json.load(f).get(apiname)
        # 定义一个新的列表
        list_data = []
        # 列表嵌套字典的测试数据转化为列表嵌套元组
        # 1.循环遍历列表嵌套字典的数据，获取每一个字典数据
        for i in api_data:
            # 2.将字典中的值提取出来，放入到元组中
            temp = tuple(i.values())
            # 3.将每一个元组，添加到新列表中
            list_data.append(temp)
    # 返回最新格式（列表嵌套元组）的测试数据
    return list_data


# html解析工具
def html_util(response):
    # 1.从响应结果中，获取请求的标签数据
    data_html = response.json().get("description").get("form")
    # 2.创建beautifulsoup对象
    bs = BeautifulSoup(data_html, "html.parser")
    # 3.从form标签中获取url
    url = bs.form.get("action")
    # 4. 从input标签中获取请求体数据
    # 4.1 定义一个字典，用来接收请求体的数据
    form_dict = dict()
    # 4.2 获取全部的input标签
    input_all = bs.findAll("input")
    # 4.3 循环遍历出每一个input标签
    for input_sample in input_all:
        # 4.4从input标签中获取请求体的字段名
        key = input_sample.get("name")
        # 4.5从input标签中获取请求体的字段值
        value = input_sample.get("value")
        # 4.6.将字段名和值放入定义的请求体数据的字典中
        form_dict[key] = value
    # 5.返回，带有url和请求体数据的列表
    return [url, form_dict]


if __name__ == '__main__':
    clear_user_info()
    print(read_json("recharge_data", "recharge"))
